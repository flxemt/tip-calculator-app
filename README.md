# Frontend Mentor - Tip calculator app solution

This is a solution to the [Tip calculator app challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/tip-calculator-app-ugJNGbJUX). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the app depending on their device's screen size
- See hover states for all interactive elements on the page
- Calculate the correct tip and total cost of the bill per person

### Screenshot

![](https://i.imgur.com/EFNWARY.png)

### Links

- Solution URL: [https://www.frontendmentor.io/solutions/tip-calculator-app-using-vanilla-js-BVSQVUaDhw](https://www.frontendmentor.io/solutions/tip-calculator-app-using-vanilla-js-BVSQVUaDhw)
- Live Site URL: [https://tip-calculator-vanilla.netlify.app/](https://tip-calculator-vanilla.netlify.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow

## Author

- Frontend Mentor - [@lumiuko](https://www.frontendmentor.io/profile/lumiuko)
- Twitter - [@lumiuko](https://twitter.com/lumiuko)

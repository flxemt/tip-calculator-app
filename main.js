const bill = document.getElementById('bill')
const peopleCount = document.getElementById('people-count')
const tipAmount = document.getElementById('tip-amount')
const total = document.getElementById('total-amount')
const customTip = document.getElementById('custom-tip')
const radioInputs = document.querySelectorAll('input[name="tip"]')
const resetBtn = document.getElementById('reset-btn')

let isCustomTip = false
let percent = 15

function validateInput(input) {
  if (Number(input.value) > Number(input.max)) {
    input.value = input.max
  } else if (Number(input.value) < Number(input.min)) {
    input.value = input.min
  }
}

function calculateTip() {
  validateInput(bill)
  validateInput(peopleCount)

  const billPerPerson = bill.value / peopleCount.value
  const tip = (billPerPerson * percent) / 100
  const totalBill = billPerPerson + Number(tip)

  tipAmount.textContent = `$${tip.toFixed(2)}`
  total.textContent = `$${totalBill.toFixed(2)}`
  resetBtn.disabled = false
}

function setCustomTip() {
  if (!isCustomTip) {
    document.querySelector('input[type="radio"]:checked').checked = false
  }

  validateInput(customTip)
  isCustomTip = true
  percent = customTip.value
  calculateTip()
}

function handleRadioInputChange(event) {
  if (isCustomTip) {
    customTip.value = ''
  }

  percent = event.target.value
  isCustomTip = false
  calculateTip()
}

function reset() {
  bill.value = ''
  peopleCount.value = ''
  tipAmount.textContent = '$0.00'
  total.textContent = '$0.00'
  resetBtn.disabled = true
}

radioInputs.forEach(input => input.addEventListener('change', handleRadioInputChange))
bill.addEventListener('input', calculateTip)
peopleCount.addEventListener('input', calculateTip)
customTip.addEventListener('input', setCustomTip)
resetBtn.addEventListener('click', reset)
